import Container from 'react-bootstrap/Container';
import {Nav, Navbar} from 'react-bootstrap'
import "bootstrap/dist/css/bootstrap.min.css";

function Menu() {
  return (
    <>
      <Navbar bg="dark" variant="dark">
        <Container>
          <Nav className="me-auto">
            <Nav.Link href="#">Inicio</Nav.Link>
            <Nav.Link href="#">Ayuda</Nav.Link>
            <Nav.Link href="#">Contactanos</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  );
}

export {Menu}